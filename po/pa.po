# Punjabi translation for gnome-documents.
# Copyright (C) 2011 gnome-documents's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-documents package.
#
# A S Alam <aalam@users.sf.net>, 2011, 2012, 2013, 2014, 2015, 2018, 2020.
msgid ""
msgstr ""
"Project-Id-Version: gnome-documents master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-documents/issues\n"
"POT-Creation-Date: 2020-05-22 06:27+0000\n"
"PO-Revision-Date: 2020-06-04 20:32-0700\n"
"Last-Translator: A S Alam <aalam.yellow@gmail.com>\n"
"Language-Team: Punjabi <kde-i18n-doc@kde.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.12.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: data/org.gnome.Documents.appdata.xml.in:7
#: data/org.gnome.Documents.desktop.in:3 src/application.js:105
#: src/overview.js:1004
msgid "Documents"
msgstr "ਡੌਕੂਮੈਂਟ"

#: data/org.gnome.Documents.appdata.xml.in:8
msgid "A document manager application for GNOME"
msgstr "ਗਨੋਮ ਲਈ ਡੌਕੂਮੈਂਟ ਮੈਨੇਜਰ ਐਪਲੀਕੇਸ਼ਨ"

#: data/org.gnome.Documents.appdata.xml.in:10
msgid ""
"A simple application to access, organize and share your documents on GNOME. "
"It is meant to be a simple and elegant replacement for using a file manager "
"to deal with documents. Seamless cloud integration is offered through GNOME "
"Online Accounts."
msgstr ""
"ਗਨੋਮ ਵਿੱਚ ਤੁਹਾਡੇ ਦਸਤਾਵੇਜ਼ (ਡੌਕੂਮੈਂਟ) ਨੂੰ ਵਰਤਣ, ਉਹਨਾਂ ਦਾ ਪਰਬੰਧ ਕਰਨ ਤੇ ਸਾਂਝਾ ਕਰਨ"
" ਲਈ ਸੌਖੀ ਜੇਹੀ "
"ਐਪਲੀਕੇਸ਼ਨ ਹੈ। ਇਸ ਨੂੰ ਦਸਤਾਵੇਜ਼ ਦਾ ਪਰਬੰਧ ਕਰਨ ਲਈ ਫਾਇਲ ਮੈਨੇਜਰ ਦੀ ਬਜਾਏ ਸੌਖਾ ਤੇ ਸਧਾਰਨ"
" ਰੂਪ ਵਿੱਚ "
"ਤਿਆਰ ਕੀਤਾ ਗਿਆ ਹੈ। ਗਨੋਮ ਆਨਲਾਈਨ ਖਾਤਿਆਂ ਰਾਹੀਂ ਕਲਾਉਡ ਜੋੜ ਨੂੰ ਵੀ ਸੌਖਾ ਬਣਾਇਆ ਗਿਆ ਹੈ।"

#: data/org.gnome.Documents.appdata.xml.in:16
msgid "It lets you:"
msgstr "ਇਹ ਤੁਹਾਡੇ ਲਈ ਹੈ:"

#: data/org.gnome.Documents.appdata.xml.in:18
msgid "View recent local and online documents"
msgstr "ਤਾਜ਼ਾ ਲੋਕਲ ਅਤੇ ਆਨਲਾਈਨ ਡੌਕੂਮੈਂਟ ਵੇਖੋ"

#: data/org.gnome.Documents.appdata.xml.in:19
msgid "Access your Google, ownCloud or OneDrive content"
msgstr "ਗੂਗਲ, ਓਵਨਕਲਾਉਡ ਜਾਂ ਵਨਡਰਾਇਵ ਸਮਗਰੀ ਵਰਤੋਂ"

#: data/org.gnome.Documents.appdata.xml.in:20
msgid "Search through documents"
msgstr "ਡੌਕੂਮੈਂਟਾਂ ਲਈ ਖੋਜ"

#: data/org.gnome.Documents.appdata.xml.in:21
msgid "See new documents shared by friends"
msgstr "ਦੋਸਤਾਂ ਵਲੋਂ ਸਾਂਝੇ ਕੀਤੇ ਨਵੇਂ ਡੌਕੂਮੈਂਟ ਵੇਖੋ"

#: data/org.gnome.Documents.appdata.xml.in:22
msgid "View documents fullscreen"
msgstr "ਡੌਕੂਮੈਂਟ ਪੂਰੀ ਸਕਰੀਨ ਉੱਤੇ ਵੇਖੋ"

#: data/org.gnome.Documents.appdata.xml.in:23
msgid "Print documents"
msgstr "ਡੌਕੂਮੈਂਟ ਪਰਿੰਟ ਕਰੋ"

#: data/org.gnome.Documents.appdata.xml.in:24
msgid "Select favorites"
msgstr "ਪਸੰਦੀਦਾ ਚੁਣੋ"

#: data/org.gnome.Documents.appdata.xml.in:25
msgid "Allow opening full featured editor for non-trivial changes"
msgstr "ਗ਼ੈਰ-ਮਮੂਲੀ ਤਬਦੀਲੀਆਂ ਲਈ ਪੂਰੇ ਫੀਚਰ ਐਡੀਟਰ ਖੋਲ੍ਹਣ ਦੀ ਮਨਜ਼ੂਰੀ"

#: data/org.gnome.Documents.appdata.xml.in:39
msgid "The GNOME Project"
msgstr "ਗਨੋਮ ਪਰੋਜੈੱਕਟ"

#: data/org.gnome.Documents.desktop.in:4
msgid "Access, manage and share documents"
msgstr "ਡੌਕੂਮੈਂਟ ਵਰਤੋਂ, ਪਰਬੰਧ ਕਰੋ ਅਤੇ ਸਾਂਝੇ ਕਰੋ"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Documents.desktop.in:15
msgid "Docs;PDF;Document;"
msgstr "ਡੌਕੂਮੈਂਟ;PDF,ਦਸਤਾਵੇਜ਼;"

#: data/org.gnome.documents.gschema.xml:5
msgid "View as"
msgstr "ਇੰਝ ਵੇਖੋ"

#: data/org.gnome.documents.gschema.xml:6
msgid "View as type"
msgstr "ਕਿਸਮਾਂ ਵਾਂਗ ਵੇਖੋ"

#: data/org.gnome.documents.gschema.xml:10
msgid "Sort by"
msgstr "ਲੜੀਬੱਧ"

#: data/org.gnome.documents.gschema.xml:11
msgid "Sort by type"
msgstr "ਲੜੀਬੱਧ ਕਿਸਮ"

#: data/org.gnome.documents.gschema.xml:15
msgid "Window size"
msgstr "ਵਿੰਡੋ ਆਕਾਰ"

#: data/org.gnome.documents.gschema.xml:16
msgid "Window size (width and height)."
msgstr "ਵਿੰਡੋ ਆਕਾਰ (ਚੌੜਾਈ ਅਤੇ ਉਚਾਈ)"

#: data/org.gnome.documents.gschema.xml:20
msgid "Window position"
msgstr "ਵਿੰਡੋ ਸਥਿਤੀ"

#: data/org.gnome.documents.gschema.xml:21
msgid "Window position (x and y)."
msgstr "ਵਿੰਡੋ ਸਥਿਤੀ (x ਅਤੇ y)"

#: data/org.gnome.documents.gschema.xml:25
msgid "Window maximized"
msgstr "ਵਿੰਡੋ ਵੱਧੋ-ਵੱਧ ਕਰਨੀ"

#: data/org.gnome.documents.gschema.xml:26
msgid "Window maximized state"
msgstr "ਵਿੰਡੋ ਵੱਧੋ-ਵੱਧ ਆਕਾਰ ਸਥਿਤੀ"

#: data/org.gnome.documents.gschema.xml:30
msgid "Night mode"
msgstr "ਰਾਤ ਮੋਡ"

#: data/org.gnome.documents.gschema.xml:31
msgid "Whether the application is in night mode."
msgstr "ਕੀ ਐਪਲੀਕੇਸ਼ਨ ਰਾਤ ਮੋਡ ਵਿੱਚ ਹੈ।"

#: data/ui/documents-app-menu.ui:6 src/preview.js:434
msgid "Night Mode"
msgstr "ਰਾਤ ਮੋਡ"

#: data/ui/documents-app-menu.ui:12
msgid "Keyboard Shortcuts"
msgstr "ਕੀ-ਬੋਰਡ ਸ਼ਾਰਟਕੱਟ"

#: data/ui/documents-app-menu.ui:16
msgid "Help"
msgstr "ਮੱਦਦ"

#: data/ui/documents-app-menu.ui:20
msgid "About Documents"
msgstr "ਡੌਕੂਮੈਂਟਸ ਬਾਰੇ"

#: data/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "ਆਮ"

#: data/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Show help"
msgstr "ਮਦਦ ਵੇਖਾਓ"

#: data/ui/help-overlay.ui:22
msgctxt "shortcut window"
msgid "Quit"
msgstr "ਬਾਹਰ"

#: data/ui/help-overlay.ui:29
msgctxt "shortcut window"
msgid "Search"
msgstr "ਖੋਜੋ"

#: data/ui/help-overlay.ui:36
msgctxt "shortcut window"
msgid "Print the current document"
msgstr "ਮੌਜੂਦਾ ਦਸਤਾਵੇਜ਼ ਨੂੰ ਛਾਪੋ"

#: data/ui/help-overlay.ui:45
msgctxt "shortcut window"
msgid "Navigation"
msgstr "ਨੇਵੀਗੇਸ਼ਨ"

#: data/ui/help-overlay.ui:49 data/ui/help-overlay.ui:57
msgctxt "shortcut window"
msgid "Go back"
msgstr "ਪਿੱਛੇ ਜਾਓ"

#: data/ui/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Preview"
msgstr "ਝਲਕ"

#: data/ui/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "ਜ਼ੂਮ ਇਨ"

#: data/ui/help-overlay.ui:78
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "ਜ਼ੂਮ ਆਉਟ"

#: data/ui/help-overlay.ui:85
msgctxt "shortcut window"
msgid "Bookmark the current page"
msgstr "ਮੌਜੂਦਾ ਸਫ਼ੇ ਨੂੰ ਬੁੱਕਮਾਰਕ ਕਰੋ"

#: data/ui/help-overlay.ui:92
msgctxt "shortcut window"
msgid "Open places and bookmarks dialog"
msgstr "ਥਾਵਾਂ ਅਤੇ ਬੁੱਕਮਾਰਕ ਡਾਈਲਾਗ ਖੋਲ੍ਹੋ"

#: data/ui/help-overlay.ui:99
msgctxt "shortcut window"
msgid "Copy selected text to clipboard"
msgstr "ਚੁਣੀ ਲਿਖਤ ਨੂੰ ਕਲਿੱਪਬੋਰਡ ਵਿੱਚ ਕਾਪੀ ਕਰੋ"

#: data/ui/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Rotate counterclockwise"
msgstr "ਖੱਬੇ ਦਾਅ ਘੁੰਮਾਓ"

#: data/ui/help-overlay.ui:113
msgctxt "shortcut window"
msgid "Rotate clockwise"
msgstr "ਸੱਜੇ ਦਾਅ ਘੁੰਮਾਓ"

#: data/ui/help-overlay.ui:120
msgctxt "shortcut window"
msgid "Next occurrence of the search string"
msgstr "ਖੋਜ ਸਤਰ ਦੀ ਅਗਲੀ ਮੌਜੂਦਗੀ"

#: data/ui/help-overlay.ui:127
msgctxt "shortcut window"
msgid "Previous occurrence of the search string"
msgstr "ਖੋਜ ਦੀ ਸਤਰ ਦੀ ਪਿਛਲੀ ਮੌਜੂਦਗੀ"

#: data/ui/help-overlay.ui:134
msgctxt "shortcut window"
msgid "Presentation mode"
msgstr "ਪੇਸ਼ਕਾਰੀ ਢੰਗ"

#: data/ui/help-overlay.ui:141
msgctxt "shortcut window"
msgid "Open action menu"
msgstr "ਕਾਰਵਾਈ ਮੇਨੂ ਖੋਲ੍ਹੋ"

#: data/ui/help-overlay.ui:148
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "ਪੂਰੀ ਸਕਰੀਨ"

#: data/ui/organize-collection-dialog.ui:40
msgid "Enter a name for your first collection"
msgstr "ਆਪਣੇ ਪਹਿਲੇ ਭੰਡਾਰ ਲਈ ਨਾਂ ਦਿਓ"

#: data/ui/organize-collection-dialog.ui:56
#: data/ui/organize-collection-dialog.ui:113
msgid "New Collection…"
msgstr "ਨਵਾਂ ਭੰਡਾਰ…"

#: data/ui/organize-collection-dialog.ui:73
#: data/ui/organize-collection-dialog.ui:122 src/sharing.js:219
msgid "Add"
msgstr "ਸ਼ਾਮਲ"

#: data/ui/organize-collection-dialog.ui:163 data/ui/selection-toolbar.ui:88
#: src/overview.js:1007 src/search.js:113
msgid "Collections"
msgstr "ਭੰਡਾਰ"

#: data/ui/organize-collection-dialog.ui:167 src/overview.js:575
msgid "Cancel"
msgstr "ਰੱਦ ਕਰੋ"

#. Label for Done button in Sharing dialog
#: data/ui/organize-collection-dialog.ui:173 src/sharing.js:106
msgid "Done"
msgstr "ਮੁਕੰਮਲ"

#: data/ui/preview-context-menu.ui:6
msgid "_Copy"
msgstr "ਕਾਪੀ ਕਰੋ(_C)"

#. Translators: this is the Open action in a context menu
#: data/ui/preview-menu.ui:6 data/ui/selection-toolbar.ui:9
#: src/selections.js:929
msgid "Open"
msgstr "ਖੋਲ੍ਹੋ"

#: data/ui/preview-menu.ui:10
msgid "Edit"
msgstr "ਸੋਧ"

#: data/ui/preview-menu.ui:15
msgid "Print…"
msgstr "…ਪਰਿੰਟ ਕਰੋ"

#: data/ui/preview-menu.ui:21 src/preview.js:443
msgid "Fullscreen"
msgstr "ਪੂਰੀ ਸਕਰੀਨ"

#: data/ui/preview-menu.ui:27
msgid "Present"
msgstr "ਪਰਿਜੈਂਟ"

#: data/ui/preview-menu.ui:35
msgid "Zoom In"
msgstr "ਜ਼ੂਮ ਇਨ"

#: data/ui/preview-menu.ui:41
msgid "Zoom Out"
msgstr "ਜ਼ੂਮ ਆਉਟ"

#: data/ui/preview-menu.ui:49
msgid "Rotate ↶"
msgstr "↶ ਘੁੰਮਾਓ"

#: data/ui/preview-menu.ui:55
msgid "Rotate ↷"
msgstr "↷ ਘੁੰਮਾਓ"

#: data/ui/preview-menu.ui:62 data/ui/selection-toolbar.ui:79
#: src/properties.js:56
msgid "Properties"
msgstr "ਵਿਸ਼ੇਸ਼ਤਾ"

#: data/ui/selection-menu.ui:6
msgid "Select All"
msgstr "ਸਭ ਚੁਣੋ"

#: data/ui/selection-menu.ui:11
msgid "Select None"
msgstr "ਸਭ ਚੁਣੋ"

#: data/ui/view-menu.ui:23
msgid "View items as a grid of icons"
msgstr "ਆਈਟਮਾਂ ਨੂੰ ਆਈਕਾਨ ਦੇ ਗਰਿੱਡ ਵਜੋਂ ਵੇਖੋ"

#: data/ui/view-menu.ui:40
msgid "View items as a list"
msgstr "ਆਈਟਮਾਂ ਨੂੰ ਸੂਚੀ ਵਜੋਂ ਵੇਖੋ"

#: data/ui/view-menu.ui:73
msgid "Sort"
msgstr "ਲੜੀਬੱਧ"

#: data/ui/view-menu.ui:84
msgid "Author"
msgstr "ਲੇਖਕ"

#: data/ui/view-menu.ui:93
msgid "Date"
msgstr "ਮਿਤੀ"

#: data/ui/view-menu.ui:102
msgid "Name"
msgstr "ਨਾਂ"

#: src/application.js:115
msgid "Show the version of the program"
msgstr "ਪਰੋਗਰਾਮ ਦਾ ਵਰਜ਼ਨ ਵੇਖੋ।"

#: src/documents.js:754
msgid "Failed to print document"
msgstr "ਡੌਕੂਮੈਂਟ ਪਰਿੰਟ ਕਰਨ ਲਈ ਫੇਲ੍ਹ"

#. Translators: this refers to local documents
#: src/documents.js:791 src/search.js:410
msgid "Local"
msgstr "ਲੋਕਲ"

#. Translators: Documents ships a "Getting Started with Documents"
#. tutorial PDF. The "GNOME" string below is displayed as the author name
#. of that document, and doesn't normally need to be translated.
#: src/documents.js:834
msgid "GNOME"
msgstr "ਗਨੋਮ"

#: src/documents.js:835
msgid "Getting Started with Documents"
msgstr "ਡੌਕੂਮੈਂਟ ਨਾਲ ਸ਼ੁਰੂਆਤ"

#: src/documents.js:859 src/documents.js:1027 src/documents.js:1124
#: src/documents.js:1250
msgid "Collection"
msgstr "ਭੰਡਾਰ"

#. overridden
#: src/documents.js:939
msgid "Google Docs"
msgstr "ਗੂਗਲ ਡੌਕਸ"

#: src/documents.js:940
msgid "Google"
msgstr "ਗੂਗਲ"

#: src/documents.js:1029 src/documents.js:1252
msgid "Spreadsheet"
msgstr "ਸਪਰੈਡਸ਼ੀਟ"

#: src/documents.js:1031 src/documents.js:1254 src/presentation.js:39
msgid "Presentation"
msgstr "ਪਰਿਜ਼ੈੱਨਟੇਸ਼ਨ"

#: src/documents.js:1033 src/documents.js:1256
msgid "Document"
msgstr "ਡੌਕੂਮੈਂਟ"

#. overridden
#: src/documents.js:1080
msgid "ownCloud"
msgstr "ownCloud"

#. overridden
#: src/documents.js:1179 src/documents.js:1180
msgid "OneDrive"
msgstr "OneDrive"

#: src/documents.js:1425
msgid "Please check the network connection."
msgstr "ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨ ਚੈਕ ਕਰੋ ਜੀ।"

#: src/documents.js:1428
msgid "Please check the network proxy settings."
msgstr "ਨੈੱਟਵਰਕ ਪਰਾਕਸੀ ਸੈਟਿੰਗ ਜਾਂਚੋ।"

#: src/documents.js:1431
msgid "Unable to sign in to the document service."
msgstr "ਡੌਕੂਮੈਂਟ ਸਰਵਿਸ ਵਿੱਚ ਸਾਇਨ ਇਨ ਕਰਨ ਲਈ ਅਸਮਰੱਥ ਹੈ।"

#: src/documents.js:1434
msgid "Unable to locate this document."
msgstr "ਡੌਕੂਮੈਂਟ ਲੱਭਣ ਲਈ ਅਸਮਰੱਥ ਹੈ।"

#: src/documents.js:1437
#, javascript-format
msgid "Hmm, something is fishy (%d)."
msgstr "ਹੂੰ,  ਕੁਝ ਗੜਬੜ ਲੱਗਦਾ ਹੈ (%d)"

#: src/documents.js:1443
msgid ""
"LibreOffice support is not available. Please contact your system "
"administrator."
msgstr "ਲਿਬਰੇਆਫ਼ਿਸ ਸਹਿਯੋਗ ਉਪਲਬਧ ਨਹੀਂ ਹੈ। ਆਪਣੇ ਸਿਸਟਮ ਪਰਸ਼ਾਸ਼ਕ ਨਾਲ ਸੰਪਰਕ ਕਰੋ।"

#. Translators: %s is the title of a document
#: src/documents.js:1466
#, javascript-format
msgid "Oops! Unable to load “%s”"
msgstr "ਓਹ! “%s” ਲੋਡ ਕਰਨ ਲਈ ਅਸਮਰੱਥ"

#. view button, on the right of the toolbar
#: src/edit.js:133
msgid "View"
msgstr "ਵੇਖੋ"

#: src/evinceview.js:567 src/lib/gd-places-bookmarks.c:656
msgid "Bookmarks"
msgstr "ਬੁੱਕਮਾਰਕ"

#: src/evinceview.js:575
msgid "Bookmark this page"
msgstr "ਇਹ ਸਫ਼ਾ ਬੁੱਕਮਾਰਕ ਕਰੋ"

#: src/lib/gd-nav-bar.c:242
#, c-format
msgid "Page %u of %u"
msgstr "%2$u ਵਿੱਚੋਂ %1$u ਸਫ਼ਾ"

#: src/lib/gd-pdf-loader.c:142
msgid "Unable to load the document"
msgstr "ਡੌਕੂਮੈਂਟ ਲੋਡ ਕਰਨ ਲਈ ਅਸਮਰੱਥ"

#. Translators: %s is the number of the page, already formatted
#. * as a string, for example "Page 5".
#.
#: src/lib/gd-places-bookmarks.c:321
#, c-format
msgid "Page %s"
msgstr "ਪੇਜ਼ %s"

#: src/lib/gd-places-bookmarks.c:384
msgid "No bookmarks"
msgstr "ਬੁੱਕਮਾਰਕ ਨਹੀਂ"

#: src/lib/gd-places-bookmarks.c:392 src/lib/gd-places-links.c:257
msgid "Loading…"
msgstr "…ਲੋਡ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ"

#: src/lib/gd-places-links.c:342
msgid "No table of contents"
msgstr "ਕੋਈ ਤਤਕਰਾ ਨਹੀਂ"

#: src/lib/gd-places-links.c:514
msgid "Contents"
msgstr "ਸਮੱਗਰੀ"

#: src/lib/gd-utils.c:318
msgid "translator-credits"
msgstr ""
"ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ\n"
"ਪੰਜਾਬੀ ਓਪਨਸੋਰਸ ਟੀਮ (POST) ੨੦੧੧-੨੦੨੦"

#: src/lib/gd-utils.c:319
msgid "A document manager application"
msgstr "ਡੌਕੂਮੈਂਟ ਮੈਨੇਜਰ ਐਪਲੀਕੇਸ਼ਨ"

#: src/mainToolbar.js:87
#| msgid "View Menu"
msgctxt "menu button tooltip"
msgid "Menu"
msgstr "ਮੇਨੂ"

#: src/mainToolbar.js:97
msgctxt "toolbar button tooltip"
msgid "Search"
msgstr "ਖੋਜੋ"

#: src/mainToolbar.js:106
msgid "Back"
msgstr "ਪਿੱਛੇ"

#. Translators: only one item has been deleted and %s is its name
#: src/notifications.js:46
#, javascript-format
msgid "“%s” deleted"
msgstr "“%s” ਹਟਾਈ"

#. Translators: one or more items might have been deleted, and %d
#. is the count
#: src/notifications.js:50
#, javascript-format
msgid "%d item deleted"
msgid_plural "%d items deleted"
msgstr[0] "%d ਆਈਟਮ ਹਟਾਈ"
msgstr[1] "%d ਆਈਟਮਾਂ ਹਟਾਈਆਂ"

#: src/notifications.js:59 src/selections.js:358
msgid "Undo"
msgstr "ਵਾਪਿਸ"

#: src/notifications.js:148
#, javascript-format
msgid "Printing “%s”: %s"
msgstr "“%s” ਪਰਿੰਟ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ: %s"

#: src/notifications.js:202
msgid "Your documents are being indexed"
msgstr "ਤੁਹਾਡੇ ਡੌਕੂਮੈਂਟ ਦਾ ਇੰਡੈਕਸ ਬਣਾਇਆ ਜਾ ਰਿਹਾ ਹੈ"

#: src/notifications.js:203
msgid "Some documents might not be available during this process"
msgstr "ਕੁਝ ਡੌਕੂਮੈਂਟ ਇਹ ਕਾਰਵਾਈ ਦੌਰਾਨ ਸ਼ਾਇਦ ਉਪਲੱਬਧ ਨਹੀਂ ਹੋਣਗੇ"

#. Translators: %s refers to an online account provider, e.g.
#. "Google", or "Windows Live".
#: src/notifications.js:225
#, javascript-format
msgid "Fetching documents from %s"
msgstr "%s ਤੋਂ ਡੌਕੂਮੈਂਟ ਲਏ ਜਾ ਰਹੇ ਹਨ"

#: src/notifications.js:227
msgid "Fetching documents from online accounts"
msgstr "ਆਨਲਾਈਨ ਅਕਾਊਂਟ ਤੋਂ ਡੌਕੂਮੈਂਟ ਲਏ ਜਾ ਰਹੇ ਹਨ"

#: src/overview.js:263
msgid "No collections found"
msgstr "ਕੋਈ ਭੰਡਾਰ ਨਹੀਂ ਲੱਭਿਆ"

#: src/overview.js:265
msgid "No documents found"
msgstr "ਕੋਈ ਦਸਤਾਵੇਜ਼ ਨਹੀਂ ਲੱਭਿਆ"

#: src/overview.js:274
msgid "Try a different search"
msgstr "ਵੱਖਰੀ ਖੋਜ ਨਾਲ ਕੋਸ਼ਿਸ਼ ਕਰੋ"

#: src/overview.js:279
msgid "You can create collections from the Documents view"
msgstr "ਤੁਸੀਂ ਦਸਤਾਵੇਜ਼ ਝਲਕ ਤੋਂ ਭੰਡਾਰ ਬਣਾ ਸਕਦੇ ਹੋ"

#: src/overview.js:284
#, javascript-format
msgid ""
"Documents from your <a href=\"system-settings\">Online Accounts</a> and <a "
"href=\"file://%s\">Documents folder</a> will appear here."
msgstr ""
"ਤੁਹਾਡੇ <a href=\"system-settings\">ਆਨਲਾਈਨ ਖਾਤਿਆਂ</a> ਅਤੇ <a href=\"file://%s"
"\">ਦਸਤਾਵੇਜ਼ ਫੋਲਡਰ</a> ਤੋਂ ਦਸਤਾਵੇਜ਼ ਇੱਥੇ ਦਿਖਾਈ ਦੇਣਗੇ।"

#. Translators: this is the menu to change view settings
#: src/overview.js:522
msgid "View Menu"
msgstr "ਮੇਨੂ ਦੇਖੋ"

#: src/overview.js:549
msgid "Click on items to select them"
msgstr "ਆਈਟਮਾਂ ਨੂੰ ਚੁਣਨ ਲਈ ਉਹਨਾਂ ਉੱਤੇ ਕਲਿੱਕ ਕਰੋ"

#: src/overview.js:551
#, javascript-format
msgid "%d selected"
msgid_plural "%d selected"
msgstr[0] "%d ਚੁਣਿਆ"
msgstr[1] "%d ਚੁਣੇ"

#: src/overview.js:631
msgid "Select Items"
msgstr "ਆਈਟਮਾਂ ਚੁਣੋ"

#: src/overview.js:833
msgid "Yesterday"
msgstr "ਕੱਲ੍ਹ"

#: src/overview.js:835
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d ਦਿਨ ਪਹਿਲਾਂ"
msgstr[1] "%d ਦਿਨ ਪਹਿਲਾਂ"

#: src/overview.js:839
msgid "Last week"
msgstr "ਪਿਛਲੇ ਹਫ਼ਤੇ"

#: src/overview.js:841
#, javascript-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "%d ਹਫ਼ਤਾ ਪਹਿਲਾਂ"
msgstr[1] "%d ਹਫ਼ਤੇ ਪਹਿਲਾਂ"

#: src/overview.js:845
msgid "Last month"
msgstr "ਪਿਛਲੇ ਮਹੀਨੇ"

#: src/overview.js:847
#, javascript-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "%d ਮਹੀਨਾ ਪਹਿਲਾਂ"
msgstr[1] "%d ਮਹੀਨੇ ਪਹਿਲਾਂ"

#: src/overview.js:851
msgid "Last year"
msgstr "ਪਿਛਲੇ ਸਾਲ"

#: src/overview.js:853
#, javascript-format
msgid "%d year ago"
msgid_plural "%d years ago"
msgstr[0] "%d ਸਾਲ ਪਹਿਲਾਂ"
msgstr[1] "%d ਸਾਲ ਪਹਿਲਾਂ"

#: src/password.js:38
msgid "Password Required"
msgstr "ਪਾਸਵਰਡ ਚਾਹੀਦਾ ਹੈ"

#: src/password.js:41
msgid "_Unlock"
msgstr "ਅਣ-ਲਾਕ(_U)"

#: src/password.js:57
#, javascript-format
msgid "Document %s is locked and requires a password to be opened."
msgstr "ਡੌਕੂਮੈਂਟ “%s” ਲਾਕ ਹੈ ਅਤੇ ਇਸ ਨੂੰ ਖੋਲ੍ਹਣ ਲਈ ਪਾਸਵਰਡ ਦੀ ਲੋੜ ਹੈ।"

#: src/password.js:71
msgid "_Password"
msgstr "ਪਾਸਵਰਡ(_P)"

#: src/presentation.js:95
msgid "Running in presentation mode"
msgstr "ਪਰਿਜ਼ੈਂਟੇਸ਼ਨ ਮੋਡ ਵਿੱਚ ਚੱਲ ਰਿਹਾ ਹੈ"

#: src/presentation.js:121
msgid "Present On"
msgstr "ਪਰਿਜੈਂਟ ਚਾਲੂ"

#. Translators: "Mirrored" describes when both displays show the same view
#: src/presentation.js:157
msgid "Mirrored"
msgstr "ਮਿਰਰ ਕੀਤਾ"

#: src/presentation.js:159
msgid "Primary"
msgstr "ਪ੍ਰਾਇਮਰੀ"

#: src/presentation.js:161
msgid "Off"
msgstr "ਬੰਦ"

#: src/presentation.js:163
msgid "Secondary"
msgstr "ਸੈਕੰਡਰੀ"

#. Translators: this is the Open action in a context menu
#: src/preview.js:426 src/selections.js:926
#, javascript-format
msgid "Open with %s"
msgstr "%s ਨਾਲ ਖੋਲ੍ਹੋ"

#: src/preview.js:751
msgid "Find Previous"
msgstr "ਪਿੱਛੇ ਖੋਜ"

#: src/preview.js:757
msgid "Find Next"
msgstr "ਅੱਗੇ ਖੋਜ"

#. Title item
#. Translators: "Title" is the label next to the document title
#. in the properties dialog
#: src/properties.js:76
msgctxt "Document Title"
msgid "Title"
msgstr "ਟਾਈਟਲ"

#. Translators: "Author" is the label next to the document author
#. in the properties dialog
#: src/properties.js:85
msgctxt "Document Author"
msgid "Author"
msgstr "ਲੇਖਕ"

#. Source item
#: src/properties.js:92
msgid "Source"
msgstr "ਸਰੋਤ"

#. Date Modified item
#: src/properties.js:98
msgid "Date Modified"
msgstr "ਸੋਧ ਮਿਤੀ"

#: src/properties.js:105
msgid "Date Created"
msgstr "ਬਣਾਉਣ ਮਿਤੀ"

#. Document type item
#. Translators: "Type" is the label next to the document type
#. (PDF, spreadsheet, ...) in the properties dialog
#: src/properties.js:114
msgctxt "Document Type"
msgid "Type"
msgstr "ਕਿਸਮ"

#. Translators: "Type" refers to a search filter on the document type
#. (PDF, spreadsheet, ...)
#: src/search.js:108
msgctxt "Search Filter"
msgid "Type"
msgstr "ਕਿਸਮ"

#. Translators: this refers to documents
#: src/search.js:111 src/search.js:218 src/search.js:404
msgid "All"
msgstr "ਸਭ"

#: src/search.js:117
msgid "PDF Documents"
msgstr "PDF ਡੌਕੂਮੈਂਟ"

#: src/search.js:122
msgid "Presentations"
msgstr "ਪਰਿਜੈੱਟੇਸ਼ਨ"

#: src/search.js:125
msgid "Spreadsheets"
msgstr "ਸਪਰੈਡਸ਼ੀਟ"

#: src/search.js:128
msgid "Text Documents"
msgstr "ਟੈਕਸਟ ਡੌਕੂਮੈਂਟ"

#. Translators: this is a verb that refers to "All", "Title", "Author",
#. and "Content" as in "Match All", "Match Title", "Match Author", and
#. "Match Content"
#: src/search.js:215
msgid "Match"
msgstr "ਮਿਲਦਾ"

#. Translators: "Title" refers to "Match Title" when searching
#: src/search.js:221
msgctxt "Search Filter"
msgid "Title"
msgstr "ਟਾਈਟਲ"

#. Translators: "Author" refers to "Match Author" when searching
#: src/search.js:224
msgctxt "Search Filter"
msgid "Author"
msgstr "ਲੇਖਕ"

#. Translators: "Content" refers to "Match Content" when searching
#: src/search.js:227
msgctxt "Search Filter"
msgid "Content"
msgstr "ਸਮੱਗਰੀ"

#: src/search.js:400
msgid "Sources"
msgstr "ਸਰੋਤ"

#. Translators: the first %s is an online account provider name,
#. e.g. "Google". The second %s is the identity used to log in,
#. e.g. "foo@gmail.com".
#: src/search.js:456
#, javascript-format
msgid "%s (%s)"
msgstr "%s (%s)"

#: src/selections.js:331 src/selections.js:333
msgid "Rename…"
msgstr "…ਨਾਂ ਬਦਲੋ"

#: src/selections.js:337 src/selections.js:339
msgid "Delete"
msgstr "ਹਟਾਓ"

#: src/selections.js:353
#, javascript-format
msgid "“%s” removed"
msgstr "“%s” ਨੂੰ ਹਟਾਇਆ"

#: src/selections.js:731
msgid "Rename"
msgstr "ਨਾਂ ਬਦਲੋ"

#. Translators: "Collections" refers to documents in this context
#: src/selections.js:737
msgctxt "Dialog Title"
msgid "Collections"
msgstr "ਭੰਡਾਰ"

#: src/sharing.js:102
msgid "Sharing Settings"
msgstr "ਸਾਂਝ ਸੈਟਿੰਗ"

#. Label for widget group for changing document permissions
#: src/sharing.js:139
msgid "Document permissions"
msgstr "ਡੌਕੂਮੈਂਟ ਅਧਿਕਾਰ"

#. Label for permission change in Sharing dialog
#: src/sharing.js:146 src/sharing.js:323
msgid "Change"
msgstr "ਬਦਲੋ"

#. Label for radiobutton that sets doc permission to private
#: src/sharing.js:170 src/sharing.js:298
msgid "Private"
msgstr "ਪ੍ਰਾਈਵੇਟ"

#: src/sharing.js:180 src/sharing.js:291
msgid "Public"
msgstr "ਪਬਲਿਕ"

#. Label for checkbutton that sets doc permission to Can edit
#: src/sharing.js:184 src/sharing.js:293
msgid "Everyone can edit"
msgstr "ਹਰੇਕ ਸੋਧ ਸਕਦਾ ਹੈ"

#. Label for widget group used for adding new contacts
#: src/sharing.js:191
msgid "Add people"
msgstr "ਲੋਕ ਸ਼ਾਮਲ"

#. Editable text in entry field
#: src/sharing.js:198
msgid "Enter an email address"
msgstr "ਈਮੇਲ ਐਡਰੈਸ ਦਿਉ"

#: src/sharing.js:212 src/sharing.js:362
msgid "Can edit"
msgstr "ਸੋਧ ਸਕਦੇ"

#: src/sharing.js:212 src/sharing.js:365
msgid "Can view"
msgstr "ਵੇਖ ਸਕਦੇ"

#: src/sharing.js:295
msgid "Everyone can read"
msgstr "ਹਰੇਕ ਪੜ੍ਹ ਸਕਦਾ"

#: src/sharing.js:310
msgid "Save"
msgstr "ਸੰਭਾਲੋ"

#: src/sharing.js:359
msgid "Owner"
msgstr "ਮਾਲਕ"

#: src/sharing.js:426
#, javascript-format
msgid "You can ask %s for access"
msgstr "ਤੁਸੀਂ %s ਲਈ ਵਰਤੋਂ ਦੀ ਪੁੱਛ ਸਕਦੇ ਹੋ"

#: src/sharing.js:463 src/sharing.js:499 src/sharing.js:556 src/sharing.js:573
#: src/sharing.js:592
msgid "The document was not updated"
msgstr "ਡੌਕੂਮੈਂਟ ਅੱਪਡੇਟ ਨਹੀਂ ਹੈ"

#: src/shellSearchProvider.js:274
msgid "Untitled Document"
msgstr "ਬਿਨ-ਨਾਂ ਡੌਕੂਮੈਂਟ"

#: src/trackerController.js:170
msgid "Unable to fetch the list of documents"
msgstr "ਡੌਕੂਮੈਂਟ ਦੀ ਲਿਸਟ ਲੈਣ ਲਈ ਅਸਮਰੱਥ"

#~ msgid "Books"
#~ msgstr "ਕਿਤਾਬਾਂ"

#~ msgid "An e-book manager application for GNOME"
#~ msgstr "ਗਨੋਮ ਲਈ ਈ-ਬੁੱਕ ਮੈਨੇਜਰ ਐਪਲੀਕੇਸ਼ਨ ਹੈ"

#~ msgid ""
#~ "A simple application to access and organize your e-books on GNOME. It is "
#~ "meant to be a simple and elegant replacement for using a file manager to "
#~ "deal with e-books."
#~ msgstr ""
#~ "ਗਨੋਮ ਵਿੱਚ ਤੁਹਾਡੀਆਂ ਈ-ਬੁੱਕਾਂ ਨੂੰ ਵਰਤਣ ਤੇ ਉਹਨਾਂ ਦਾ ਪਰਬੰਧ ਕਰਨ ਲਈ ਸੌਖੀ ਜੇਹੀ ਐਪਲੀਕੇਸ਼ਨ ਹੈ। ਇਸ ਨੂੰ "
#~ "ਈ-ਬੁੱਕਾਂ ਦਾ ਪਰਬੰਧ ਕਰਨ ਲਈ ਫਾਇਲ ਮੈਨੇਜਰ ਨੂੰ ਸੌਖਾ ਤੇ ਸਧਾਰਨ ਰੂਪ ਵਿੱਚ ਤਿਆਰ ਕੀਤਾ ਗਿਆ ਹੈ।"

#~ msgid "View recent e-books"
#~ msgstr "ਤਾਜ਼ਾ ਈ-ਕਿਤਾਬਾਂ ਵੇਖੋ"

#~ msgid "Search through e-books"
#~ msgstr "ਈ-ਬੁੱਕਾਂ ਵਿੱਚ ਲੱਭੋ"

#~| msgid "View e-books fullscreen"
#~ msgid "View e-books (PDF and comics) fullscreen"
#~ msgstr "ਈ-ਬੁੱਕਾਂ (PDF ਅਤੇ ਹਾਸਰਸ ਕਿਤਾਬਾਂ) ਨੂੰ ਪੂਰੀ ਸਕਰੀਨ ਉੱਤੇ ਵੇਖੋ"

#~ msgid "Print e-books"
#~ msgstr "ਈ-ਬੁੱਕ ਛਾਪੋ"

#~ msgid "Access, manage and share books"
#~ msgstr "ਕਿਤਾਬਾਂ ਨੂੰ ਵਰਤੋਂ, ਪਰਬੰਧ ਕਰੋ ਅਤੇ ਸਾਂਝੀਆਂ ਕਰੋ"

#~ msgid "org.gnome.Books"
#~ msgstr "org.gnome.clocks"

#~ msgid "Books;Comics;ePub;PDF;"
#~ msgstr "ਕਿਤਾਬਾਂ;ਕੌਮਿਕਸ;ePub;PDF;ਈ-ਪਬ;ਪੀਡੀਐਫ;"

#~| msgid "Documents"
#~ msgid "org.gnome.Documents"
#~ msgstr "org.gnome.Documents"

#~ msgid "About"
#~ msgstr "ਇਸ ਬਾਰੇ"

#~| msgid "Quit"
#~ msgctxt "app menu"
#~ msgid "Quit"
#~ msgstr "ਬਾਹਰ"

#~ msgid "e-Book"
#~ msgstr "ਈ-ਬੁੱਕ"

#~ msgid ""
#~ "You are using a preview of Books. Full viewing capabilities are coming "
#~ "soon!"
#~ msgstr "ਤੁਸੀਂ ਕਿਤਾਬਾਂ ਦੀ ਝਲਕ ਵਰਤ ਰਹੇ ਹੋ। ਪੂਰੀ ਝਲਕ ਦੀ ਸਮੱਰਥਾ ਛੇਤੀ ਹੀ ਆ ਰਹੀ ਹੈ!"

#~| msgid "Page %u of %u"
#~ msgid "chapter %s of %s"
#~ msgstr "%2$s ਵਿੱਚੋਂ %1$s ਕਾਂਡ"

#~ msgid "An e-books manager application"
#~ msgstr "ਈ-ਬੁੱਕ ਮੈਨੇਜਰ ਐਪਲੀਕੇਸ਼ਨ"

#~| msgid "No Books Found"
#~ msgid "No books found"
#~ msgstr "ਕੋਈ ਕਿਤਾਬ ਨਹੀਂ ਲੱਭੀ"

#~ msgid "You can create collections from the Books view"
#~ msgstr "ਤੁਸੀਂ ਕਿਤਾਬ ਝਲਕ ਤੋਂ ਭੰਡਾਰ ਬਣਾ ਸਕਦੇ ਹੋ"

#~ msgid "e-Books"
#~ msgstr "ਈ-ਬੁੱਕ"

#~ msgid "Comics"
#~ msgstr "ਕਾਮਿਕ"

#~| msgid "New and Recent"
#~ msgid "Recent"
#~ msgstr "ਤਾਜ਼ਾ"

#~ msgid "LibreOffice is required to view this document"
#~ msgstr "ਇਹ ਡੌਕੂਮੈਂਟ ਵੇਖਣ ਲਈ ਲਿਬਰੇਆਫਿਸ ਦੀ ਲੋੜ ਹੈ"

#~ msgid "Category"
#~ msgstr "ਕੈਟਾਗਰੀ"

#~ msgid "Favorites"
#~ msgstr "ਪਸੰਦੀਦਾ"

#~ msgid "Shared with you"
#~ msgstr "ਤੁਹਾਡੇ ਨਾਲ ਸਾਂਝਾ ਕੀਤਾ"

#~ msgid ""
#~ "You don't have any collections yet. Enter a new collection name above."
#~ msgstr "ਤੁਹਾਡੇ ਕੋਲ ਕੋਈ ਵੀ ਭੰਡਾਰ ਨਹੀਂ ਹੈ। ਉੱਤੇ ਨਵੇਂ ਭੰਡਾਰ ਲਈ ਨਾਂ ਦਿਉ।"

#~ msgid "Create new collection"
#~ msgstr "ਨਵਾਂ ਭੰਡਾਰ ਬਣਾਓ"

#~ msgid "Print"
#~ msgstr "ਪਰਿੰਟ ਕਰੋ"

#~ msgid "Share"
#~ msgstr "ਸਾਂਝਾ"

#~ msgid "Add to Collection"
#~ msgstr "ਭੰਡਾਰ ਵਿੱਚ ਸ਼ਾਮਲ"

#~ msgid "You can add your online accounts in %s"
#~ msgstr "ਤੁਸੀਂ %s ਵਿੱਚ ਆਪਣੇ ਆਨਲਾਈਨ ਅਕਾਊਂਟ ਸ਼ਾਮਿਲ ਕਰ ਸਕਦੇ ਹੋ"

#~ msgid "Settings"
#~ msgstr "ਸੈਟਿੰਗ"

#~ msgid "Load More"
#~ msgstr "ਹੋਰ ਲੋਡ"

#~| msgid ""
#~| "<li>View recent local and online documents</li> <li>Access your Google, "
#~| "ownCloud or SkyDrive content</li> <li>Search through documents</li> "
#~| "<li>See new documents shared by friends</li> <li>View documents "
#~| "fullscreen</li> <li>Print documents</li> <li>Select favorites</li> "
#~| "<li>Allow opening full featured editor for non-trivial changes</li>"
#~ msgid ""
#~ "<li>View recent local and online documents</li> <li>Access your Google, "
#~ "ownCloud or OneDrive content</li> <li>Search through documents</li> "
#~ "<li>See new documents shared by friends</li> <li>View documents "
#~ "fullscreen</li> <li>Print documents</li> <li>Select favorites</li> "
#~ "<li>Allow opening full featured editor for non-trivial changes</li>"
#~ msgstr ""
#~ "<li>ਲੋਕਲ ਅਤੇ ਆਨਲਾਈਨ ਡੌਕੂਮੈਂਟ ਵੇਖਾਉਣ ਲਈ</li> <li>ਤੁਹਾਡੇ ਗੂਗਲ, ਓਵਨਕਲਾਊਡ ਜਾਂ ਵਨਡਰਾਇਵ "
#~ "ਸਮੱਗਰੀ ਵਰਤਣ ਲਈ</li> <li>ਡੌਕੂਮੈਂਟ (ਦਸਤਾਵੇਜ਼) ਵਿੱਚ ਲੱਭਣ ਲਈ</li> <li>ਦੋਸਤਾਂ ਵਲੋਂ ਸਾਂਝੇ ਕੀਤੇ "
#~ "ਨਵੇਂ ਦਸਤਾਵੇਜ਼ ਵੇਖਾਉਣ ਲਈ</li><li>ਦਸਤਾਵੇਜ਼ ਪੂਰੀ ਸਕਰੀਨ ਉੱਤੇ ਵੇਖਾਉਣ ਲਈ</li><li>ਦਸਤਾਵੇਜ਼ "
#~ "ਪਰਿੰਟ ਕਰਨ ਲਈ</li><li>ਪਸੰਦ ਚੁਣਨ ਲਈ</li><li>ਗੈਰ-ਮਾਮੂਲੀ ਬਦਲਾਅ ਲਈ ਪੂਰੇ ਫੀਚਰ ਭਰਪੂਰ "
#~ "ਐਡੀਟਰ ਨਾਲ ਖੋਲ੍ਹਣ ਦੇਣ ਲਈ</li>"

#~ msgid "Results for “%s”"
#~ msgstr "“%s” ਲਈ ਨਤੀਜੇ"

#~ msgid "Close"
#~ msgstr "ਬੰਦ ਕਰੋ"

#~ msgid "Skydrive"
#~ msgstr "ਸਕਾਈਡਰਾਇਵ"

#~ msgid "Grid"
#~ msgstr "ਗਰਿੱਡ"

#~ msgid "List"
#~ msgstr "ਸੂਚੀ"

#~ msgid "Organize"
#~ msgstr "ਪਰਬੰਧ"

#~ msgid "filtered by title"
#~ msgstr "ਨਾਂ ਮੁਤਾਬਕ ਲੜੀਬੱਧ"

#~ msgid "filtered by author"
#~ msgstr "ਲੇਖਕ ਮੁਤਾਬਕ ਲੜੀਬੱਧ"

#~ msgid "Unable to load \"%s\" for preview"
#~ msgstr "\"%s\" ਦੀ ਝਲਕ ਲੋਡ ਕਰਨ ਲਈ ਅਸਮਰੱਥ"

#~ msgid "Cannot find \"unoconv\", please check your LibreOffice installation"
#~ msgstr ""
#~ "\"unoconv\" ਨਹੀ ਲੱਭਿਆ ਜਾ ਸਕਦਾ, ਆਪਣੀ ਲਿਬਰੇਆਫਿਸ (LibreOffice) ਇੰਸਟਾਲੇਸ਼ਨ ਜਾਂਚੋ"

#~ msgid "Load %d more document"
#~ msgid_plural "Load %d more documents"
#~ msgstr[0] "%d ਹੋਰ ਡੌਕੂਮੈਂਟ ਲੋਡ ਕਰੋ"
#~ msgstr[1] "%d ਹੋਰ ਡੌਕੂਮੈਂਟ ਲੋਡ ਕਰੋ"

#~ msgid "Print..."
#~ msgstr "...ਪਰਿੰਟ ਕਰੋ"

#~ msgid "Rotate Right"
#~ msgstr "ਸੱਜੇ ਘੁੰਮਾਓ"

#~ msgid "The active source filter"
#~ msgstr "ਸਰਗਰਮ ਸਰੋਤ ਫਿਲਟਰ"

#~ msgid "The last active source filter"
#~ msgstr "ਆਖਰੀ ਸਰਗਰਮ ਸਰੋਤ ਫਿਲਟਰ"

#~| msgid "(%d of %d)"
#~ msgid "%d of %d"
#~ msgstr "%2$d ਵਿੱਚੋਂ %1$d"

#~| msgid "Documents"
#~ msgid "GNOME Documents"
#~ msgstr "ਗਨੋਮ ਡੌਕੂਮੈਂਟ"

#~ msgid "Remove from favorites"
#~ msgstr "ਪਸੰਦ ਵਿੱਚੋਂ ਹਟਾਓ"

#~ msgid "Add to favorites"
#~ msgstr "ਪਸੰਦ ਵਿੱਚ ਸ਼ਾਮਲ ਕਰੋ"

#~ msgid "Enable list view"
#~ msgstr "ਲਿਸਟ ਝਲਕ ਚਾਲੂ"
